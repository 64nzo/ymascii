YMASCII PROJECT - Anzo. - INDEV 0.0
===============================================================================

A somewhat "clone" of Yume Nikki, but in an terminal interface
way inspired by other ascii games such as rogue or dwarf forteress.

BUILDING AND RUNNING
-------------------------------------------------------------------------------
The only dependency for now is the libc, and as such should compile in any
Unix system, and work in any terminal that support **true colors**

As such, simply building the project with
`make`
should provide you a working binary.

Support for Windows systems is not supported yet but planned, 
(I'll try to add that with Cygwin.)

XOXO
