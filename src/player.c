/*Copyright 2023-2024 Anzo.
This file is part of the YMASCII PROJECT, and as such, is under the 
GPL-3.0-or-later licence.
Please refer to the LICENCE file for licence details.*/

#include "player.h"
#include "entity.h"

#include "globals.h"

//@DEPRECEATED
void movePlayerInLevel(int _row, int _col)
{
	//had weird issues.. prolly shouldn't mix unsigned and signed int qwq
	//we suppose map infinite.
	if (PLAYER.row == 0 && _row == -1)
	{PLAYER.row = CURRENT_LEVEL->row - 1;}
	else
	{PLAYER.row = ((PLAYER.row + _row) % CURRENT_LEVEL->row);}

	if (PLAYER.col == 0 && _col == -1)
	{PLAYER.col = CURRENT_LEVEL->col -1;}
	else
	{PLAYER.col = ((PLAYER.col + _col) % CURRENT_LEVEL->col);} 
} 

void playerInteract(entity_t*self){return;} 


//TODO : make so the speed is limited
void playerProcess(entity_t* self)
{
	Key input = getKeyInput();
	int _row = 0;
	int _col = 0;
	switch (input)
	{
		case K_NOKEY:
			break;
		case K_UP:
			_row = -1;
			break;
		case K_DOWN:
			_row = 1;
			break;
		case K_LEFT:
			_col = 1;
			break;
		case K_RIGHT:
			_col = -1;
			break;
		//FIXME move this from somewhere else (disembody key input and this)
		case K_SCROLL_UP:
			TEXT_SCROLL = 
				(TEXT_SCROLL+1 > MAX_MESSAGES-1) ? MAX_MESSAGES-1 : TEXT_SCROLL+1;
			//printf("TEXT_SCROLL %i", TEXT_SCROLL);
			break;
		case K_SCROLL_DOWN:
			TEXT_SCROLL = (TEXT_SCROLL > 0) ? TEXT_SCROLL-1 : 0;
			//printf("TEXT_SCROLL %i", TEXT_SCROLL);
			break;

		case K_DEBUG:
			DEBUG_FUN();
			break;	
		
		case K_INVALID:
				sendMessageFmt("INVALID KEY GOT");
				break;

		case K_MOUSE_LPRS:
			sendMessageFmt("MOUSE LEFT PRESS : r%u c%u", MOUSE_ROW, MOUSE_COL);
			break;
	}
	//printf("\033[0;0H%i\n%i",player.col,player.row);
	moveEntityInLevel(self,_col,_row);	
} 

entity_t PLAYER =
{
	.color = {.bg = {0,0,0}, .fg = {255,255,0}}
 ,.appearance = '@'
 ,.col = 0
 ,.row = 0
 ,.z = 1
 ,.process = &playerProcess
 ,.interact = &playerInteract
 ,.args = NULL
 ,.free = NULL
};
