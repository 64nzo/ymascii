/*Copyright 2023-2024 Anzo.
This file is part of the YMASCII PROJECT, and as such, is under the 
GPL-3.0-or-later licence.
Please refer to the LICENCE file for licence details.*/

//entities
#ifndef ENTITY_H_
#define ENTITY_H_

#include <string.h>
#include <stdlib.h>

#include "render.h"

typedef struct entity_t
{
	color_t color;
	char appearance;
	unsigned col; //position
	unsigned row;
	int z; //z-layers
	void (*process)(struct entity_t* self);
	void (*interact)(struct entity_t* self);
	void *args; //propreties, arguments, anything more data specialised..
	void (*free)(struct entity_t* self);//free function, default it to NULL
} entity_t; 

//chained list of entities
struct entity_list_t
{
	entity_t* e;
	struct entity_list_t* next;
};
typedef struct entity_list_t entity_list_t;

//free itself. to use when malloced.
void freeEntity(entity_t*self);

//free the entity list.
void freeEntityList(entity_list_t*);

//append an element to the chained list.
void appendEntity(entity_list_t** list, entity_t* e);

//call all process() of all entities in a given entity list
void processEntitylist(entity_list_t* list);

//put entity appearance with right color on designated canvas.
//coordinate are mod (%) through the canvas dimensions.
void renderEntity(entity_t,canvas_t*);

//same as above, but for all of the entities.. coordinates of the list
//are mod % through the canvas dimensions to avoid segfaults..
//be cautious of this.
void renderEntitylits(entity_list_t *, canvas_t*);

//moves entity while staying in boundaries of CURRENT_LEVEL (wrapping)
void moveEntityInLevel(entity_t*e, int col, int row);

//render entity in the CURRENT_LEVEL in respect to the z layer.
void renderEntityCurrentLevel(entity_t*);

//render entity list in the CURRENT_LEVEL in respect to the z layer.
void renderEntitylistCurrentLevel(entity_list_t*);

#endif //ENTITY_H_
