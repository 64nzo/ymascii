/*Copyright 2023-2024 Anzo.
This file is part of the YMASCII PROJECT, and as such, is under the 
GPL-3.0-or-later licence.
Please refer to the LICENCE file for licence details.*/

//User input

#ifndef INPUT_H_
#define INPUT_H_

#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<string.h>

#include "messages.h"

//All possible key events
typedef enum
{
	K_NOKEY,
	K_RIGHT,
	K_LEFT,
	K_UP,
	K_DOWN,
	K_INVALID,
	K_SCROLL_UP,
	K_SCROLL_DOWN,
	K_DEBUG,
	K_MOUSE_LPRS, //PRS and release mouse clicks.
	K_MOUSE_RPRS,
	K_MOUSE_LRLS,
	K_MOUSE_RRLS,
} Key;

extern unsigned int MOUSE_ROW;
extern unsigned int MOUSE_COL;

//return key input from user. returns K_NOKEY if nothing was pressed.
Key getKeyInput();

#endif //INPUT_H_
