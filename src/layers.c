/*Copyright 2023-2024 Anzo.
This file is part of the YMASCII PROJECT, and as such, is under the 
GPL-3.0-or-later licence.
Please refer to the LICENCE file for licence details.*/

#include "layers.h"

#include "render.h"
#include "entity.h"
#include "globals.h"

canvas_t GAME_CANVAS;
canvas_t TEXT_CANVAS;

void initGameCanvas()
{
	unsigned grow = 3*(ROW-3)/4;
	unsigned gcol = COL-2;
	initCanvas(&GAME_CANVAS, gcol, grow);
	setCanvasToColor(&GAME_CANVAS,'.',GRAY_ON_BLACK);
}

void initTextCanvas()
{
	unsigned trow = ROW-(GAME_CANVAS.row+3);
	unsigned tcol = COL-2;
	initCanvas(&TEXT_CANVAS, tcol, trow);
	setCanvasToColor(&TEXT_CANVAS,' ',GRAY_ON_BLACK);
}
