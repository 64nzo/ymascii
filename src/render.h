/*Copyright 2023-2024 Anzo.
This file is part of the YMASCII PROJECT, and as such, is under the 
GPL-3.0-or-later licence.
Please refer to the LICENCE file for licence details.*/

//Rendering stuff on terminal

#ifndef RENDER_H_
#define RENDER_H_

#include<stdio.h>
#include<sys/ioctl.h>
#include<termios.h>
#include<unistd.h>

#ifdef EDITOR
static const int COL = 100;
static const int ROW = 40;
#else
static const int COL = 60;
static const int ROW = 30;
#endif

typedef struct canvas_t
{
	char* mem;
	int col;
	int row;
} canvas_t;

extern canvas_t SCREEN_CANVAS;
extern canvas_t DEBUG_CANVAS;

// '\033[38;2;000;000;000;48;2;255;255;255m@' is a 37 char long word 
static const size_t coloredChar_size = 37;

//silly constants for more verbose code.
#define R 0
#define G 1
#define B 2

//color with both bg and fg info
typedef struct color_t
{
	unsigned char bg[3];//rgb
	unsigned char fg[3];//rgb
} color_t;

#define COLOR(Rb, Gb, Bb, Rf, Gf, Bf) {.bg={Rb,Gb,Bb},.fg={Rf,Gf,Bf}}
//return col but, for each value, xg[x] + (rand % x) 
color_t rndColDev(color_t col
		, unsigned char Rb, unsigned char Gb, unsigned char Bb
		, unsigned char Rf, unsigned char Gf, unsigned char Bf);

//previous term settings
static struct termios term_prev;
//prev winsize
static struct winsize w_prev;

void setTerminalConfig();
void resetTerminalConfig();
void verifyScreenSize();
void verifyResize();

void initCanvas(canvas_t* canvas, int col, int row);
void initRender();

int isEqArray(char*,char*,int);

void renderScreenFull();
void renderScreen();
void blitCanvas(canvas_t* in, canvas_t* out, unsigned int pos_col
		, unsigned int pos_row);
void blitCanvasCentered(canvas_t* in, canvas_t *out);

//blits *in onto *out, with centered around pcol and prow (in *in)
void blitCanvasCenteredWrapping(canvas_t* in, canvas_t* out,int pcol
		, int prow);

//takes a char and color, returns esc seq.
void charColorToESC(char* out, char, color_t); 
void setCanvasToColor(canvas_t*, char, color_t);
void renderOrnament();
void fadeToBlack(canvas_t*);//blocks everything - do a nice black fading on the
														//designated canvas.

//debug fun
void debugScreen();
void debugBlit();
void debugDumpCanvas();

#endif //RENDER_H_
