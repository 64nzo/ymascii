/*Copyright 2023-2024 Anzo.
This file is part of the YMASCII PROJECT, and as such, is under the 
GPL-3.0-or-later licence.
Please refer to the LICENCE file for licence details.*/

#include "input.h"

unsigned int MOUSE_ROW;
unsigned int MOUSE_COL;
char buff[16]; //16 char buffer... 

Key extractMouseInfo(char* buff)
{
	//sendMessageFmt("extracting mouse input %s", buff);
	int mPress = 0; // 0 is rls, 1 is press
	int mButton = 0; // 0 is leftclick, 2 right click
		
	mButton = atoi(strtok(buff,";"));

	MOUSE_COL = atoi(strtok(NULL,";"));
	
	buff = strtok(NULL,";");	
	int len = strlen(buff);
	mPress = buff[len-1] == 'M' ? 1 : 0;
	buff[len-1] = '\0';
	
	MOUSE_ROW = atoi(buff);
	//sendMessageFmt("mPress:%i, mButton:%i, col:%i, row:%i"
	//		, mPress, mButton, MOUSE_COL, MOUSE_ROW);

	if (mPress)
	{
		if (mButton == 2)
		{return K_MOUSE_RPRS;}
		else
		{return K_MOUSE_LPRS;} 	
	} 
	else
	{
		if (mButton == 2)
		{return K_MOUSE_RRLS;}
		else
		{return K_MOUSE_LRLS;}
	}

	return K_INVALID;
}

//return key input from user. returns K_NOKEY if nothing was pressed.
Key getKeyInput()
{
	//char to put read from STDIN
	char r_key = '\0';
	int r_size;

	r_size = read(STDIN_FILENO, &r_key, sizeof(char));
	if (r_size)
	{
		switch (r_key)
		{
			case 'P':
				return K_DEBUG;
			case 'z':
				return K_UP;
			case 's':
				return K_DOWN;
			case 'd':
				return K_LEFT;
			case 'q':
				return K_RIGHT;
			case 'u':
				return K_SCROLL_UP;
			case 'i':
				return K_SCROLL_DOWN;
			case 27: //esc sequence shit
				read(STDIN_FILENO, &r_key, sizeof(char));
				if (r_key != '[')
					return K_INVALID;
				read(STDIN_FILENO, &r_key, sizeof(char));
				switch(r_key)
				{
					case 'A':
						return K_UP;
					case 'B':
						return K_DOWN;
					case 'C':
						return K_LEFT;
					case 'D':
						return K_RIGHT;
					case '<': //mouse mvmt !!!!!!!
						read(STDIN_FILENO, buff, sizeof(char)*16);
						return extractMouseInfo(buff);
				}
		} 
	}
	else
	{ 
		return K_NOKEY;
	}
}
