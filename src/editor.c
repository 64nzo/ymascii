/*Copyright 2023-2024 Anzo.
This file is part of the YMASCII PROJECT, and as such, is under the 
GPL-3.0-or-later licence.
Please refer to the LICENCE file for licence details.*/

/**
 * Simple level editor.
 */

#include<stdio.h>
#include<sys/ioctl.h>
#include<termios.h>
#include<unistd.h>
#include<stdint.h>
#include<signal.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<string.h>

#include "globals.h"

#include "input.h"
#include "render.h"
#include "layers.h"
#include "messages.h"
#include "level.h"

void (*DEBUG_FUN)(void);
size_t cC = coloredChar_size;

int FREE_OLD_LEVEL=0; //unused but wtvr

int fd_commands;
char buffer[255];
char* editorPipe = "./editor_command";

char currentCol[37];
canvas_t palette_canvas;

void sigcatch()
{
	resetTerminalConfig(); //restor term mode
	printf("\033[200H"); // move all the way down
	printf("\033[?25h"); //restore cursor visibility
	printf("\033[?1000l\033[?1006l");//disable mouse trap

	exit(1);
} 

unsigned mouseCcol; //mouse col in canvas
unsigned mouseCrow; //mouse row in canvas

//Absolute pos to relative pos
void absToRelGameCanvas(unsigned col, unsigned row
		, unsigned* outc, unsigned* outr)
{

	unsigned tmpr, tmpc;
	
	struct winsize w;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	tmpc = (w.ws_col/2) - (COL/2) + 1; //where col
	tmpr = (w.ws_row/2) - (ROW/2) + 1; //where row
	//+1 is to put us at the game canvas start
	
	//sendMessageFmt("%u %u", tmpc, tmpr);

	canvas_t cl = CURRENT_LEVEL->canvas;

	tmpc += ( GAME_CANVAS.col - cl.col > 0) ? (GAME_CANVAS.col - cl.col)/2 : 0;
	tmpr += ( GAME_CANVAS.row - cl.row > 0) ? (GAME_CANVAS.row - cl.row)/2 : 0;
	
	//sendMessageFmt("%u %u", tmpc, tmpr);
		
	*outc = col - tmpc - 1;
	*outr = row - tmpr - 1;

	//sendMessageFmt("%u %u", col-1, row-1);
} 

void initGame()
{
	signal(SIGINT, sigcatch); //catches ctrl+c and such

	printf("\033[1J\033[H"); //clear screen, move cursor up
	printf("\033[?25l");// hides cursor FIXME non standard, idk what it does
											// when unsupported.
	printf("\033[?1000h\033[?1006h");//enable mouse trap. FIXME 
																	 //non standard either.

	setTerminalConfig();	
	initRender();
	verifyScreenSize();
	
	initCanvas(&palette_canvas, 5,1);
	setCanvasToColor(&palette_canvas, '-', GRAY_ON_GRAYER);

	initGameCanvas();
	setCanvasToColor(&GAME_CANVAS, ' ', GRAY_ON_BLACK);
	
	initTextCanvas();
	setCanvasToColor(&SCREEN_CANVAS, '#', GRAY_ON_GRAYER);
;	
	initMessagesQueue();
	
	//creating pipe to receive commands
	mkfifo(editorPipe, 0600);
	fd_commands = open(editorPipe, O_RDONLY | O_NONBLOCK);
} 

//process custom commands put through editor_command pipe
void editorCommand()
{
	ssize_t size;
	
	memset(buffer, 0, sizeof(char)*255);
	size = read(fd_commands, buffer, sizeof(char)*255);
	if (size > 0)
	{
		char * _cmd = strtok(buffer,"\n");
		sendMessageFmt("[command] %s:", _cmd);
		_cmd = strtok(_cmd," ");
		
		if (_cmd == NULL)
		{_cmd = buffer;}

		if (strcmp(_cmd,"uwu") == 0)
		{
			sendMessageFmt("OwO");
			return;
		}
		if (strcmp(_cmd, "color") == 0)
		{
			_cmd = strtok(NULL,"\0");
			memcpy(currentCol, _cmd, 37);
			sendMessageFmt("Changed current color.", currentCol);
			return;	
		} 
		sendMessageFmt("command unknown :(");	
	} 
} 

//process user inputs.
void editorKeyProc()
{
	Key input = getKeyInput();
	
	/*
	if (input)
	{ sendMessageFmt("input :%u", input); }
	*/

	switch(input)
	{
		case K_MOUSE_LPRS:
			absToRelGameCanvas(MOUSE_COL, MOUSE_ROW, &mouseCcol, &mouseCrow);
			//check inbound	
			if (mouseCcol >= 0 && mouseCcol < CURRENT_LEVEL->col 
					&& mouseCrow >= 0 && mouseCrow < CURRENT_LEVEL->row)
			{ 
				//change appearance
				memcpy(CURRENT_LEVEL->appearance+(posCL(mouseCcol,mouseCrow)*cC)
					, currentCol
					, cC);
			}
			//sendMessageFmt("MOUSE LEFT PRESS : r%u c%u", mouseCrow, mouseCcol);
			break;

		default:
			break;	
	} 
} 

void _debug_main(void)
{
	sendMessage("DEBUG MESSAGE");
} 

void editor_init(void);
void editor_process(void);

level_t editor_frame = 
{
		.col = 5
	, .row = 5
	, .zdepth = NULL
	, .zdepth_template = NULL
	, .collision = NULL
	, .appearance = NULL
	, .canvas = NULL
	, .entities = NULL
	, .process = &editor_process
	, .init = &editor_init 
	, .free = &freeLevel
}; 

void editor_init()
{
	CURRENT_LEVEL = &editor_frame;	
	
	setCanvasToColor(&CURRENT_LEVEL->canvas,'.', GRAY_ON_BLACK);
	
	memcpy(CURRENT_LEVEL->appearance
		, (CURRENT_LEVEL->canvas).mem
		, CURRENT_LEVEL->col*CURRENT_LEVEL->row*cC);
		
} 

void editor_process()
{
	//Rendering
	memcpy((CURRENT_LEVEL->canvas).mem
		, CURRENT_LEVEL->appearance
		, CURRENT_LEVEL->col*CURRENT_LEVEL->row*cC);
	
	renderEntitylistCurrentLevel(CURRENT_LEVEL->entities);
	blitCanvasCentered(&CURRENT_LEVEL->canvas
			,&GAME_CANVAS);

	//palette_canvas rendering	
	memcpy(palette_canvas.mem, currentCol, cC);

	//command from ext
	editorCommand();

	//input
	editorKeyProc();	
} 	

int main(int argc, char** argv)
{
	initGame();
	DEBUG_FUN = &_debug_main;
	
	sendMessageFmt("EDITOR : %s %i", argv[0], getpid());
	
	Key input;
	
	renderOrnament();	
	renderScreenFull();
	initLevel(&editor_frame);
	
	while(1)
	{
		usleep(dt);
		verifyScreenSize();
		verifyResize();
		
		CURRENT_LEVEL->process();
		
		blitCanvas(&palette_canvas, &GAME_CANVAS, 0,0);
		blitCanvas(&GAME_CANVAS, &SCREEN_CANVAS,1,1);
		blitMessages(TEXT_SCROLL);
		blitCanvas(&TEXT_CANVAS, &SCREEN_CANVAS,1,2+GAME_CANVAS.row);	
		renderScreen();
	} 
	return 0;
}
