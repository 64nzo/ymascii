/*Copyright 2023-2024 Anzo.
This file is part of the YMASCII PROJECT, and as such, is under the 
GPL-3.0-or-later licence.
Please refer to the LICENCE file for licence details.*/

#include "level.h"

#include <string.h>

#include "render.h"
#include "entity.h"
#include "globals.h"
#include "messages.h"

level_t *CURRENT_LEVEL;
level_t *OLD_LEVEL;

//set memory
void mallocLevel(level_t* level)
{
	int size = level->col*level->row;

	level->zdepth = (int*)malloc(size*sizeof(int));
	memset(level->zdepth, 0, size*sizeof(int));

	level->zdepth_template = (int*)malloc(size*sizeof(int));
	memset(level->zdepth_template, 0, size*sizeof(int));
	
	level->entityMap = (entity_t**)malloc(size*sizeof(entity_t*));
	for (int i=0; i<size; level->entityMap[i++] = NULL);
	
	level->collision = (int*)malloc(size*sizeof(int));
	memset(level->collision, 0, size*sizeof(int));

	level->appearance = (char *)malloc(size*coloredChar_size*sizeof(char));
	initCanvas(&(level->canvas), level->col, level->row);
}

void freeLevel(level_t* level)
{
	freeEntityList(level->entities);
	free((level->canvas).mem);
	free(level->zdepth);
	free(level->zdepth_template);
	free(level->entityMap);
	free(level->collision);
	free(level->appearance);
} 

void initLevel(level_t* level)
{
	mallocLevel(level);
	level->entities = NULL;
	(level->init)();
}

void loadLevel(level_t* level)
{
	OLD_LEVEL = CURRENT_LEVEL;
	FREE_OLD_LEVEL = 1;
	mallocLevel(level);
	initLevel(level);
}  

void updateEntityMap(level_t* level)
{
	//reset map
	for (int i=0; i<level->row*level->col; level->entityMap[i++] = NULL);
	entity_list_t* runner = level->entities;
	while (runner != NULL)
	{
		level->entityMap[runner->e->row*level->col + runner->e->col] = runner->e; 
		runner = runner->next;
	} 
}

void resetZdepth(level_t* level)
{
	int size = level->col*level->row;
	memcpy(level->zdepth, level->zdepth_template, size*sizeof(int));
} 

int posCL(int col, int row)
{
	return col + row*CURRENT_LEVEL->col;
} 

/********************************LEVELS***************************************/
//Note that levels included here have to be declared in the header file too.
#include "levels/lvl_TEST0.c"
#include "levels/lvl_NEXUS.c"
/*****************************************************************************/
