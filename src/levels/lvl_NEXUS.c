/*Copyright 2023-2024 Anzo.
This file is part of the YMASCII PROJECT, and as such, is under the 
GPL-3.0-or-later licence.
Please refer to the LICENCE file for licence details.*/

#include "../level.h"
#include "../layers.h"
#include "../render.h"
#include "../messages.h"
#include "../globals.h"
#include "../entity.h"
#include "../player.h"
#include "../render.h"

#include <stdlib.h>
#include <string.h>

#undef LVL_NAME
#define LVL_NAME NEXUS

#include "tp.c"

void LVL(process)(void);
void LVL(init)(void);

level_t NEXUS =
{
		.col = 50
	, .row = 50
	, .zdepth = NULL
	, .zdepth_template = NULL
	, .collision = NULL
	, .appearance = NULL
	, .canvas = NULL
	, .entities = NULL
	, .process = &LVL(process)
	, .init = &LVL(init)
	, .free = &freeLevel
};

void LVL(DEBUG)()
{
	sendMessageFmt("Hello %c. %i, %i", PLAYER.appearance
			, PLAYER.col, PLAYER.row);
} 

entity_t LVL(tpTEST0);
const color_t LVL(DOOR0) = COLOR(173,132,101,255,208,126);

void LVL(init)(void)
{
	CURRENT_LEVEL = &NEXUS;	
	DEBUG_FUN = &LVL(DEBUG);

	char tmp[37];
	size_t cC = coloredChar_size;
	
	for (int i=0; i < NEXUS.row*NEXUS.col; i++)
	{
		charColorToESC(tmp,(char) (i % 93)+32,GRAY_ON_BLACK);	
		memcpy(NEXUS.appearance+(i*coloredChar_size), tmp, coloredChar_size);  
	} 
		
	sendMessage("We are in the Nexus.");
	
	//to bunnyland
	CURRENT_LEVEL->zdepth_template[posCL(1,0)]=2;	
	initTP(&LVL(tpTEST0), 1, 1, &TEST0, TEST0_door_col, TEST0_door_row+1);
	//end bunnyland
	
	appendEntity(&CURRENT_LEVEL->entities, &PLAYER);
	appendEntity(&CURRENT_LEVEL->entities, &LVL(tpTEST0));
}

void LVL(process)(void)
{
	int size = NEXUS.col * NEXUS.row;
	size_t cC = coloredChar_size;
	color_t bg_color = COLOR(0,0,0,0,0,40);
	
	//noisy background	
	char tmp[37];
	char *crand = "     #";
	char c;
	static int timer = -1;
	
	timer = (timer+1) % 2;
	if (!timer)
	{ 
		for (int i=0 ; i < size ; i++)
		{
			c = crand[rand()%6]; 
			charColorToESC(tmp,c,rndColDev(bg_color,20,0,40,20,20,40));
			memcpy((CURRENT_LEVEL->appearance)+(i*coloredChar_size)
					,tmp
					,coloredChar_size);
		} 
	}
	//end noisy background
	//bunnyland door
	charColorToESC(tmp, ' ', LVL(DOOR0));
	memcpy(CURRENT_LEVEL->appearance+(posCL(1,0)*cC), tmp, cC);
	charColorToESC(tmp, '~', LVL(DOOR0));
	memcpy(CURRENT_LEVEL->appearance+(posCL(1,1)*cC), tmp, cC);
	//end bunnyland door

	memcpy((CURRENT_LEVEL->canvas).mem
		, CURRENT_LEVEL->appearance
		, CURRENT_LEVEL->col*CURRENT_LEVEL->row*coloredChar_size);

	resetZdepth(CURRENT_LEVEL);
	
	updateEntityMap(CURRENT_LEVEL);	
	processEntitylist(CURRENT_LEVEL->entities);
	
	renderEntitylistCurrentLevel(CURRENT_LEVEL->entities);
	blitCanvasCenteredWrapping(&CURRENT_LEVEL->canvas
			,&GAME_CANVAS
			,PLAYER.col
			,PLAYER.row);
	return;
} 
