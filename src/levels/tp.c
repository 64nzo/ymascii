/*Copyright 2023-2024 Anzo.
This file is part of the YMASCII PROJECT, and as such, is under the 
GPL-3.0-or-later licence.
Please refer to the LICENCE file for licence details.*/

//teleporter entity template

struct LVL(warp_info)
{
	int col;
	int row;
	level_t* warp;
};

void LVL(tpProcess)(entity_t*self)
{return;}

void LVL(tpInteract)(entity_t*self)
{
	struct LVL(warp_info)* args = (struct LVL(warp_info)*) self->args;
	loadLevel(args->warp);
	PLAYER.col = args->col;
	PLAYER.row = args->row;
	return;
}

void LVL(tpFree)(entity_t*self)
{
	struct LVL(warp_info)* args = (struct LVL(warp_info)*) self->args;
	free(args);
} 

entity_t LVL(tp_template) = 
{
	.color = GRAY_ON_BLACK
	,.appearance = 'X'
	,.col = 0
	,.row = 0
	,.z = -1 //non visible
	,.process = &LVL(tpProcess)
	,.interact = &LVL(tpInteract)
	,.args = NULL
	,.free = &LVL(tpFree)
};

#undef initTP
//initTP(entity_t*, int col, int row, level_t* warp,int dest col, int dest row)
#define initTP(tp,col_,row_,warp_,dcol,drow) \
{ \
	memcpy(tp,&LVL(tp_template),sizeof(entity_t));\
	(tp)->col = col_;\
	(tp)->row = row_;\
	(tp)->args = (void*)malloc(sizeof(struct LVL(warp_info)));\
	struct LVL(warp_info)* args = (struct LVL(warp_info)*) (tp)->args;\
	args->col = dcol;\
	args->row = drow;\
	args->warp = warp_;\
}
