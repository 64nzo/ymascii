/*Copyright 2023-2024 Anzo.
This file is part of the YMASCII PROJECT, and as such, is under the 
GPL-3.0-or-later licence.
Please refer to the LICENCE file for licence details.*/

#include "../level.h"
#include "../layers.h"
#include "../render.h"
#include "../messages.h"
#include "../globals.h"
#include "../entity.h"
#include "../player.h"

#include <stdlib.h>
#include <string.h>

#undef LVL_NAME
#define LVL_NAME TEST0

#include "tp.c"

void LVL(process)(void);
void LVL(init)(void);

level_t LVL_NAME =
{
		.col = 100
	, .row = 50
	, .zdepth = NULL
	, .zdepth_template = NULL
	, .collision = NULL
	, .appearance = NULL
	, .canvas = NULL
	, .entities = NULL
	, .process = &LVL(process)
	, .init = &LVL(init)
	, .free = &freeLevel
};

void LVL(bnuyProcess)(entity_t*self)
{
	int rand_ = rand() % 100;
	int mvc = 0;
	int mvr = 0;
	if (rand_ > 96)
	{
		mvc = (rand() % 3) - 1;
		mvr = (rand() % 3) - 1;
	}
	moveEntityInLevel(self,mvc,mvr);	
} 

void LVL(bnuyInteract)(entity_t*self)
{
	sendMessage("*boing*");
}

entity_t LVL(bnuy) =
{
	.color = COLOR(210,205,200,0,0,0)
	,.appearance = 'b'
	,.col = 0
	,.row = 0
	,.z = 1
	,.process = &LVL(bnuyProcess)
	,.interact = &LVL(bnuyInteract)
	,.args = NULL
	,.free = &freeEntity
};

void LVL(initBnuys)()
{
	int numBnuys = 25;
	int rand_;
	for (int i=0; i<numBnuys; i++)
	{
		entity_t* bnuy = (entity_t*) malloc(sizeof(entity_t));
		memcpy(bnuy,&LVL(bnuy),sizeof(entity_t));
		bnuy->col = rand()%CURRENT_LEVEL->col;
		bnuy->row = rand()%CURRENT_LEVEL->row;
		bnuy->color = rndColDev(LVL(bnuy).color,30,10,0,30,5,0);
		appendEntity(&CURRENT_LEVEL->entities,bnuy);
	} 
} 

//FIXME only works in truecolor
color_t LVL(GREEN_GRASS) = COLOR(78,136,70,0,124,66);
color_t LVL(BLUE_FLOWER) = COLOR(78,136,70,119,63,230);

char LVL(grass_light)[37]; 
char LVL(grass_heavy)[37];
char LVL(grass_sheavy)[37];
char LVL(grass_flower)[37];

void LVL(green_field)()
{
	//fills the background with some green fields..
	int size = CURRENT_LEVEL->col * CURRENT_LEVEL->row;
	char tmp[37]; 	
	for (int i = 0; i<size; i++)
	{
		
		float random = rand() / (double) RAND_MAX;
		if (random > 0.96) //{tmp = LVL(grass_flower);}   
		{charColorToESC(tmp, '*', rndColDev(LVL(BLUE_FLOWER),0,2,15,65,0,25));}
		else if (random > 0.80) //{tmp = LVL(grass_heavy);}
		{charColorToESC(tmp, 'v', rndColDev(LVL(GREEN_GRASS),0,3,1,0,15,15));}
		else if (random > 0.05) //{tmp = LVL(grass_light);}
		{charColorToESC(tmp, ' ', rndColDev(LVL(GREEN_GRASS),0,3,1,0,15,15));}
		else if (random > 0) //{tmp = LVL(grass_sheavy);}
		{charColorToESC(tmp, 'V', rndColDev(LVL(GREEN_GRASS),0,3,1,0,15,15));}
		
		//char c;
		//sprintf(&c,"%i",i%1);
		//charColorToESC(tmp,c,LVL(GREEN_GRASS));
		
		memcpy(CURRENT_LEVEL->appearance+(i*coloredChar_size)
					, tmp
					, coloredChar_size);
	}
} 

//door
const unsigned LVL(door_col) = 50;
const unsigned LVL(door_row) = 25;
const color_t LVL(DOOR) = COLOR(173,132,101,255,208,126);

entity_t LVL(door); 
//end door

void LVL(DEBUG)(void)
{
	sendMessageFmt("Player pos : c%i, r%i", PLAYER.col, PLAYER.row);
	//LVL(doorInteract)(&PLAYER);
} 

void LVL(init)(void)
{
	CURRENT_LEVEL = &LVL_NAME;
	
	DEBUG_FUN = &LVL(DEBUG);

	srand(0);	

	charColorToESC(LVL(grass_light), ' ', LVL(GREEN_GRASS));
	charColorToESC(LVL(grass_heavy), 'v', LVL(GREEN_GRASS));
	charColorToESC(LVL(grass_sheavy), 'V', LVL(GREEN_GRASS));
	charColorToESC(LVL(grass_flower),'*', LVL(BLUE_FLOWER));

	LVL(green_field)();

	size_t cC = coloredChar_size;
	
	//door init
	initTP(&LVL(door),LVL(door_col),LVL(door_row),&NEXUS,1,2);
	char tmp[37];
	charColorToESC(tmp, ' ', LVL(DOOR));
	memcpy(CURRENT_LEVEL->appearance+(CURRENT_LEVEL->col*(LVL(door_row)-1)*cC)
			+(LVL(door_col)*cC)
			, tmp
			, cC);
	charColorToESC(tmp, '~', LVL(DOOR));
	memcpy(CURRENT_LEVEL->appearance+(CURRENT_LEVEL->col*(LVL(door_row))*cC)
			+(LVL(door_col)*cC)
			, tmp
			, cC);
	
	appendEntity(&CURRENT_LEVEL->entities, &LVL(door));
	CURRENT_LEVEL->zdepth_template[posCL(LVL(door_col),LVL(door_row)-1)]=2;
	//the door is in front 


	//end door init

	sendMessage("There are bnuys here ! oWo");
	//sendMessage("soon, i hope.");
	
	LVL(initBnuys)();
	appendEntity(&CURRENT_LEVEL->entities, &PLAYER);
} 

void LVL(process)(void)
{
	//sendMessage("hello from TEST0");
	memcpy((CURRENT_LEVEL->canvas).mem
			, CURRENT_LEVEL->appearance
			, CURRENT_LEVEL->col*CURRENT_LEVEL->row*coloredChar_size);
	
	resetZdepth(CURRENT_LEVEL);
	updateEntityMap(CURRENT_LEVEL);	

	processEntitylist(CURRENT_LEVEL->entities);
		
	//renderEntity(PLAYER, &CURRENT_LEVEL->canvas);	
	renderEntitylistCurrentLevel(CURRENT_LEVEL->entities);
	blitCanvasCenteredWrapping(&CURRENT_LEVEL->canvas
			,&GAME_CANVAS
			,PLAYER.col
			,PLAYER.row);
	return;
} 
