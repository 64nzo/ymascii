/*Copyright 2023-2024 Anzo.
This file is part of the YMASCII PROJECT, and as such, is under the 
GPL-3.0-or-later licence.
Please refer to the LICENCE file for licence details.*/

//Canvas layers to show on screen
#ifndef LAYERS_H_
#define LAYERS_H_

#include "render.h"

extern canvas_t GAME_CANVAS;
extern canvas_t TEXT_CANVAS;

void initGameCanvas();
void initTextCanvas();

#endif //LAYERS_H_
