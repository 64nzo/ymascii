/*Copyright 2023-2024 Anzo.
This file is part of the YMASCII PROJECT, and as such, is under the 
GPL-3.0-or-later licence.
Please refer to the LICENCE file for licence details.*/

#include "globals.h"
#include "entity.h"
#include "messages.h"

void freeEntity(entity_t*self)
{free(self);} 

void freeEntityList(entity_list_t*list)
{
	entity_list_t* tmp;
	while (list != NULL)
	{
		tmp = list;
		list = list->next;
		if (tmp->e->free != NULL)
		{tmp->e->free(tmp->e);}
		free(tmp);
	} 
} 

void appendEntity(entity_list_t **list, entity_t* e)
{
	if (*list == NULL)//first elm init
	{
		*list = malloc(sizeof(entity_list_t));
		(*list)->e = e;
		(*list)->next = NULL;
		return;
	}
	
	entity_list_t *runner = *list;	
	//get onto the last elm of our chained list
	while(runner->next != NULL) {runner = runner->next;}
	//we malloc the next elm
	runner->next = malloc(sizeof(entity_list_t));
	//we get onto the new, last elm
	runner = runner->next;
	//we init this last elm as required*
	runner->next = NULL;
	runner->e = e;
	/*
	char tmp[100];
	sprintf(tmp,"appended '%c'",e->appearance);	
	sendMessage(tmp);*/
} 

void processEntitylist(entity_list_t* list)
{
	while (list != NULL)
	{
		//DEBUG
		/*char tmp[100];
		sprintf(tmp,"processing '%c'",(list->e)->appearance);	
		sendMessage(tmp);*/
		(list->e)->process(list->e);
		list = list->next;
	} 
} 

void renderEntity(entity_t e, canvas_t* c)
{ 
	unsigned pcol = e.col % c->col;
	unsigned prow = e.row % c->row;
	
	char ch[coloredChar_size];
	charColorToESC(ch, e.appearance, e.color);
	memcpy(c->mem+(prow*c->col*coloredChar_size)+(pcol*coloredChar_size)
				,ch
				,coloredChar_size);
}

void renderEntitylits(entity_list_t* list, canvas_t* c)
{
	while (list != NULL)
	{
		renderEntity(*(list->e), c);
		list = list->next;
	} 
}

void renderEntityCurrentLevel(entity_t*e)
{
	int pos = e->col + (e->row)*CURRENT_LEVEL->col;
	if (CURRENT_LEVEL->zdepth[pos] < e->z)
	{
		CURRENT_LEVEL->zdepth[pos] = e->z;
		renderEntity(*e, &CURRENT_LEVEL->canvas);	
	} 
} 

void renderEntitylistCurrentLevel(entity_list_t*list)
{
	while (list != NULL)
	{
		renderEntityCurrentLevel(list->e);
		list = list->next;
	} 
} 

//TODO diagonal mvmt
//TODO collisions
void moveEntityInLevel(entity_t *e, int col, int row)
{
	//had weird issues.. prolly shouldn't mix unsigned and signed int qwq
	//we suppose map infinite.
	unsigned ncol; //new col pos
	unsigned nrow;
	if (e->row == 0 && row == -1)
	{nrow = CURRENT_LEVEL->row - 1;}
	else
	{nrow = ((e->row + row) % CURRENT_LEVEL->row);}

	if (e->col == 0 && col == -1)
	{ncol = CURRENT_LEVEL->col -1;}
	else
	{ncol = ((e->col + col) % CURRENT_LEVEL->col);}
	
	if (!col && !row){return;}//no movement, stay in position.

	if (CURRENT_LEVEL->collision[nrow*CURRENT_LEVEL->col + ncol])
	{return;} //do not move the entity there. it's a wall
		
	//TODO entities with no collision	
	entity_t* other_e = CURRENT_LEVEL->entityMap[nrow*CURRENT_LEVEL->col + ncol];	
	if ( other_e != NULL && other_e != e)//something alive is there, and isnt us.
	{
		if (e == &PLAYER)
		{other_e->interact(other_e);}
		return;
		//NOTE : other_e != e should always be true, but oh well.
	}

	e->row = nrow;
	e->col = ncol; 
} 
