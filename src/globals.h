/*Copyright 2023-2024 Anzo.
This file is part of the YMASCII PROJECT, and as such, is under the 
GPL-3.0-or-later licence.
Please refer to the LICENCE file for licence details.*/

/* GLOBALS */
#ifndef GLOBALS_H_
#define GLOBALS_H_

#include "render.h"
#include "level.h"
#include "entity.h"

extern void (*DEBUG_FUN)(void);

//refresh rate in microsecondes.
static const unsigned dt = 10000; //10ms

//COLORS
static const color_t GRAY_ON_BLACK = {.bg = {0,0,0}, .fg = {127,127,127}};
static const color_t BLUE_ON_BLUE = COLOR(51,51,123,71,71,153);
static const color_t GRAY_ON_GRAYER = COLOR(40,40,40,50,50,50);

extern level_t* CURRENT_LEVEL;
extern level_t* OLD_LEVEL;
extern int FREE_OLD_LEVEL;
extern entity_t PLAYER;
#endif// GLOBALS_H_


