/*Copyright 2023-2024 Anzo.
This file is part of the YMASCII PROJECT, and as such, is under the 
GPL-3.0-or-later licence.
Please refer to the LICENCE file for licence details.*/

//player entity
#ifndef PLAYER_H_
#define PLAYER_H_

#include "entity.h"
#include "input.h"
#include "render.h"
#include "messages.h"

extern entity_t PLAYER;

void playerProcess(entity_t* self);

#endif //PLAYER_H_
