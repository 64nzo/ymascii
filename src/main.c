/*Copyright 2023-2024 Anzo.
This file is part of the YMASCII PROJECT, and as such, is under the 
GPL-3.0-or-later licence.
Please refer to the LICENCE file for licence details.*/

/**
 * to be yume nikki clone on terminal.
 * Terminal world explorator engine.
 */

#include<stdio.h>
#include<sys/ioctl.h>
#include<termios.h>
#include<unistd.h>
#include<stdint.h>
#include <signal.h>

#include "globals.h"

#include "input.h"
#include "render.h"
#include "player.h"
#include "layers.h"
#include "messages.h"
#include "level.h"

void (*DEBUG_FUN)(void);
int FREE_OLD_LEVEL = 0;

void sigcatch()
{
	resetTerminalConfig(); //restor term mode
	printf("\033[200H"); // move all the way down
	printf("\033[?25h"); //restore cursor visibility
	printf("\033[?1000l\033[?1006l");//disable mouse trap
	CURRENT_LEVEL->free(CURRENT_LEVEL);
	exit(1);
} 

void initGame()
{
	signal(SIGINT, sigcatch); //catches ctrl+c and such

	printf("\033[1J\033[H"); //clear screen, move cursor up
	printf("\033[?25l");// hides cursor FIXME non standard, idk what it does
											// when unsupported.
	printf("\033[?1000h\033[?1006h");//enable mouse trap. FIXME 
																	 //non standard either.

	setTerminalConfig();	
	initRender();
	verifyScreenSize();

	initGameCanvas();
	initTextCanvas();

	initMessagesQueue();

	setCanvasToColor(&SCREEN_CANVAS, '#', GRAY_ON_GRAYER);
} 

void _debug_main(void)
{
	sendMessage("DEBUG TEST");
} 

int main(void)
{
	initGame();
	DEBUG_FUN = &_debug_main;

	Key input;
	
	
	renderOrnament();	
	renderScreenFull();
	initLevel(&TEST0);
	
	while(1)
	{
		usleep(dt);
		verifyScreenSize();
		verifyResize();
			
		setCanvasToColor(&GAME_CANVAS, '.', GRAY_ON_BLACK);
			
		CURRENT_LEVEL->process();
		
		if (FREE_OLD_LEVEL)// we have switch level. let the old one free
		{
			FREE_OLD_LEVEL = 0;
			OLD_LEVEL->free(OLD_LEVEL);
		} 

		blitCanvas(&GAME_CANVAS, &SCREEN_CANVAS,1,1);
		blitMessages(TEXT_SCROLL);
		blitCanvas(&TEXT_CANVAS, &SCREEN_CANVAS,1,2+GAME_CANVAS.row);	
		renderScreen();
	} 
	return 0;
}

//old debug code
/*
for(int i=0; i < 100; i++)
{
	char tmp[38]; //37 + '\0' 
	snprintf(tmp
		, coloredChar_size+1
		, "\033[38;2;000;000;000;48;2;%03d;%03d;%03dmX"
		,i%255,0,0);
memcpy((debug_canvas.mem)+(i*(coloredChar_size)),tmp,coloredChar_size);
}*/

/*
if ((stupid_timer > 100) && flag==1)
{
	sendMessages("Hi !"); 
	flag++;
}
if ((stupid_timer > 105) && flag==2)
{
	sendMessages("hru ?"); 
	flag++;
}
if ((stupid_timer > 200)&&flag==3)
{
	for (int i=0; i < 25; i++)
	{
		sendMessages("");
	}
	flag++;
}*/
