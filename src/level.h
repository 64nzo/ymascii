/*Copyright 2023-2024 Anzo.
This file is part of the YMASCII PROJECT, and as such, is under the 
GPL-3.0-or-later licence.
Please refer to the LICENCE file for licence details.*/

/* map structure */
#ifndef LEVEL_H_
#define LEVEL_H_

#include "entity.h"

//level dependent declaration
// LVL(x) -->  LEVEL_NAME_x ; avoid name collision and allow cc/cv of code.

/*
https://stackoverflow.com/questions/1489932/how-can-i-concatenate-twice-with-
the-c-preprocessor-and-expand-a-macro-as-in-ar
*/

#define LVL_NAME NO_LEVEL
#define PASTER(x,y) x ## _ ## y
#define EVALUATOR(x,y)  PASTER(x,y)
#define LVL(fun_decl) EVALUATOR(LVL_NAME, fun_decl)

typedef struct level_t
{
	//level tile dimensions
	unsigned col;
	unsigned row;

	int * collision; //col*row ints for static collision.
	int * zdepth_template;//default zdepth map of size col*row
	int * zdepth;//of size col*row

	char* appearance;//background of the map (col*row*coloredChar_size)
	canvas_t canvas;//canvas of the full level

	entity_list_t *entities;//its entities
	entity_t * *entityMap; //col*row map of pointers pointing to entities 
		
	void (*process)(void);//called each dt - special map behavior
	void (*init)(void);
	void (*free)(struct level_t*self);//callback when freeing level.
} level_t;

void mallocLevel(level_t* level);
void initLevel(level_t* level);
void freeLevel(level_t* level);
void loadLevel(level_t* level);

void updateEntityMap(level_t* level);
void resetZdepth(level_t* level);
//return col + row*CURRENT_LEVEL.col
int posCL(int col, int row);

/** Levels **/
extern level_t TEST0;
extern level_t NEXUS;

#endif //LEVEL_H_
