/*Copyright 2023-2024 Anzo.
This file is part of the YMASCII PROJECT, and as such, is under the 
GPL-3.0-or-later licence.
Please refer to the LICENCE file for licence details.*/

//Messages that appears in TEXT_CANVAS.
#ifndef MESSAGES_H_
#define MESSAGES_H_

struct messages_queue_t
{
	unsigned rank;
	char* message;
	struct messages_queue_t* next;
};
typedef struct messages_queue_t* messages_queue_t;

static messages_queue_t MESSAGES_QUEUE;
//maximum message length.
//if a mess > message_length, it is broken up in multiple messages.
extern unsigned MESSAGES_LENGTH;
//size of the queue.
static unsigned MAX_MESSAGES = 50;

//position in the message queue.
extern unsigned TEXT_SCROLL;

//init Messages queue (requires TEXT_CANVAS to be init)
void initMessagesQueue();

//add messages to queue
void addMessagesToQueue(char * mess);

//blit messages to canvas
void blitMessages(unsigned rank);

//send a new messages
void sendMessage(char* mess);

//send message with printf format
void sendMessageFmt(char *fmt, ...);

#endif //MESSAGES_H_
