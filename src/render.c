/*Copyright 2023-2024 Anzo.
This file is part of the YMASCII PROJECT, and as such, is under the 
GPL-3.0-or-later licence.
Please refer to the LICENCE file for licence details.*/

#include<stdlib.h>
#include<stdio.h>
#include<unistd.h>
#include<string.h>

#include "render.h" 

#include "layers.h"

color_t rndColDev(color_t col
		, unsigned char Rb, unsigned char Gb, unsigned char Bb
		, unsigned char Rf, unsigned char Gf, unsigned char Bf)
{
	color_t ncol;

	ncol.bg[R] = col.bg[R] + (rand() % (Rb+1));
	ncol.bg[G] = col.bg[G] + (rand() % (Gb+1));
	ncol.bg[B] = col.bg[B] + (rand() % (Bb+1));

	ncol.fg[R] = col.fg[R] + (rand() % (Rf+1));
	ncol.fg[G] = col.fg[G] + (rand() % (Gf+1));
	ncol.fg[B] = col.fg[B] + (rand() % (Bf+1));
	//overflow detection
	for (int i=0; i<3; i++)
	{
		ncol.fg[i]  = (ncol.fg[i] < col.fg[i] ) ? 255 : ncol.fg[i] ;
		ncol.bg[i]  = (ncol.bg[i] < col.bg[i] ) ? 255 : ncol.bg[i] ;
	} 
	return ncol;
}

canvas_t SCREEN_CANVAS;
canvas_t SCREEN_CANVAS_prev;

void setTerminalConfig() 
{
	struct termios term;
	tcgetattr(STDIN_FILENO, &term);
	term_prev = term;
	term.c_lflag &= ~(ICANON | ECHO);
	term.c_cc[VMIN] = 0;
	term.c_cc[VTIME] = 0; 	
	tcsetattr(STDIN_FILENO, TCSANOW, &term);
}

void resetTerminalConfig()
{
	tcsetattr(STDIN_FILENO, TCSANOW, &term_prev);
}

//wait for stdout to be the correct size.
void verifyScreenSize()
{
	struct winsize w;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	
	int b_flag = 0; 
	while (w.ws_col < COL || w.ws_row < ROW)
	{
		b_flag = 1;
		printf("\033[0m");
		printf("\033[2J\033[HRequire size of at least %dx%d\ncurrent size: %dx%d\n"
				, COL
				, ROW
				, w.ws_col
				, w.ws_row);
		ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
		usleep(50000);
	}
	//update screen only if needed
	if (b_flag) { printf("\033[2J"); renderScreenFull();}
} 

void verifyResize()
{
	struct winsize w;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	if (!(w.ws_col == w_prev.ws_col && w.ws_row == w_prev.ws_row))
	{
		//screen was resized
		printf("\033[0m\033[2J");
		renderScreenFull();
	}
 w_prev = w;	
} 

void initCanvas(canvas_t* canvas, int _col, int _row)
{
	canvas->row = _row;
	canvas->col = _col;
	canvas->mem = malloc(canvas->col*canvas->row*sizeof(char)*coloredChar_size); 
}

void initRender()
{
	initCanvas(&SCREEN_CANVAS, COL, ROW);
	initCanvas(&SCREEN_CANVAS_prev, COL, ROW);	
} 

//draws a random noise pattern all over the terminal;
void noisePatternFullscreen()
{
	srand(0);
	printf("\033[0H");
	struct winsize w;
	int rand_;
	int rbg;
	int rfg;
	color_t col_;
	char tmp[37];
	char *crand = "&#%$@";
	char rc;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	for (int i = 0; i < w.ws_col*w.ws_row ; i++)
	{;
		rbg = rand() % 10;
		rfg = rand() % 15;
		rc = crand[rand()%5];  
		col_.bg[0] = rbg; col_.bg[1] = rbg; col_.bg[2] =(int) rbg*2.2;
		col_.fg[0] = rfg; col_.fg[1] = rfg; col_.fg[2] =(int) rfg*3;
		charColorToESC(tmp,rc,col_);
		printf("%.*s",coloredChar_size,tmp);
	} 	
} 

void renderScreenFull()
{
	noisePatternFullscreen();

	char* mem = SCREEN_CANVAS.mem;
	struct winsize w;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	int w_c = (w.ws_col/2) - (COL/2); //where col
	int w_r = (w.ws_row/2) - (ROW/2); //where row
	for(int i=0; i < ROW; i++)
	{
		printf("\033[%i;%iH%.*s"
				, w_r+i+1,w_c+1
				, COL*coloredChar_size, mem+(i*coloredChar_size*COL));

		fflush(stdout);
	}
	memcpy(SCREEN_CANVAS_prev.mem, SCREEN_CANVAS.mem, ROW*COL*coloredChar_size);
}

//returns true if a[] == b[]   
int isEqArray(char* a, char* b, int n)
{
	int ret = 0;
	int i = 0;
	while ((!ret) && (i<n))
	{
		ret += (a[i] - b[i]);
		//printf("\033[%i;20H%c %c %i",i+1,a[i],b[i], i);
		//printf("\033[%i;10H%i",i+1,(!ret)||i<n);
		//printf("\033[%i;0H%i",i+1,ret);
		i++;
	}
	return (!ret);
} 

//only renders the difference.
void renderScreen()
{
	struct winsize w;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	int w_c = (w.ws_col/2) - (COL/2); //where col
	int w_r = (w.ws_row/2) - (ROW/2); //where row
	
	size_t cC = coloredChar_size;	
	int size = SCREEN_CANVAS.col * SCREEN_CANVAS.row;

	for (int i = 0; i < size; i++)
	{
		if (!isEqArray(SCREEN_CANVAS.mem+(i*cC),SCREEN_CANVAS_prev.mem+(i*cC),cC))
		{
			unsigned prow = i / SCREEN_CANVAS.col;
			unsigned pcol = i % SCREEN_CANVAS.col;
			printf("\033[%i;%iH%.*s"
			, w_r+prow+1,w_c+pcol+1
			, cC, SCREEN_CANVAS.mem+(i*cC));
			fflush(stdout);
		} 
	}
	printf("\033[0;0H"); fflush(stdout);
	memcpy(SCREEN_CANVAS_prev.mem, SCREEN_CANVAS.mem, size*cC);	
} 

void blitCanvas(canvas_t* in, canvas_t* out, unsigned int pc, unsigned int pr)
{	
	size_t cC = coloredChar_size;
	
	int rows = (out->row - pr > in->row) ? in->row : out->row - pr;
	int cols = (out->col - pc > in->col) ? in->col : out->col - pc;
	//printf("%i\n%i",rows,cols);
		
	for(int i=0; i<rows ; i++)
	{
		memcpy(
				 out->mem+((i+pr)*out->col*cC)+(pc*cC)
				, in->mem+(i*in->col*cC)
				, cC*cols*sizeof(char));
	}		
} 

void blitCanvasCentered(canvas_t* in, canvas_t* out)
{
	unsigned int pc = (out->col - in->col > 0) ? (out->col - in->col)/2 : 0;
	unsigned int pr = (out->row - in->row > 0) ? (out->row - in->row)/2 : 0;
	blitCanvas(in,out,pc,pr);		
} 

void debugScreen()
{
	char *mem = SCREEN_CANVAS.mem;
	for(int i=0; i < ROW*COL; i++)
	{
		char tmp[38]; //37 + '\0' 
		snprintf(tmp
				, coloredChar_size+1
				, "\033[48;2;000;000;000;38;2;%03d;%03d;%03dm&"
				,i%255,(i*i)%255,(i/2)%255);
		strncpy(mem+(i*coloredChar_size),tmp,coloredChar_size);
	} 
}

void debugBlit()
{
	char tmp_screen[coloredChar_size*50]; //3*2
	canvas_t debug_canvas = {.mem=tmp_screen, .col=10, .row=5};

	for(int i=0; i < 50; i++)
	{
		char tmp[38]; //37 + '\0' 
		snprintf(tmp
				, coloredChar_size+1
				, "\033[38;2;000;000;000;48;2;%03d;%03d;%03dm@"
				,i%255,i/9%255,i/12%255);
		memcpy((debug_canvas.mem)+(i*(coloredChar_size)),tmp,coloredChar_size);
	}
	blitCanvasCentered(&debug_canvas, &SCREEN_CANVAS);
	//debugDumpCanvas(&SCREEN_CANVAS,"./debug3");
}

void debugDumpCanvas(canvas_t* canvas, char *path)
{
	FILE *file = fopen(path,"w");
	for (int i=0; i<canvas->row; i++)
	{
		fwrite(canvas->mem+(i*canvas->col*coloredChar_size)
				,sizeof(char)
				,canvas->col*coloredChar_size
				,file);
		fprintf(file,"\n");
	}
 fclose(file);	
} 

void blitCanvasCenteredWrapping(canvas_t* in, canvas_t* out, int cicol
		, int cirow)
{
	int dim = out->row * out->col;
	int hrow = out->row/2;
	int hcol = out->col/2;
	int _row = 0;
	int _col = 0;
	size_t cC = coloredChar_size;
	
	//printf("\033[0H");
		
	for (int i=0 ; i < dim ; i++)
	{
		_row = i/out->col;
		_col = i%out->col;
		//use pen and paper..
		_row = (((_row + cirow - hrow) % in->row) + in->row) % in->row;
		_col = (((_col + cicol - hcol) % in->col) + in->col) % in->col;
		
		//printf("\033[0Hcopying _row = %i, _col = %i, at offset row%i, col%i\n"
		//		,_row,_col,i/out->col,i%out->col);
	
		memcpy(out->mem+(i*cC)
				, in->mem+(((_row*in->col)+_col)*cC)
				, cC);
			} 

} 

void charColorToESC(char* out, char ch, color_t co)
{
	char tmp[38];
	snprintf(tmp
		, coloredChar_size+1
		, "\033[48;2;%03d;%03d;%03d;38;2;%03d;%03d;%03dm%c"
		, co.bg[R], co.bg[G], co.bg[B]
		, co.fg[R], co.fg[G], co.fg[B]
		, ch);
	memcpy(out,tmp,coloredChar_size);
}

void setCanvasToColor(canvas_t* canvas, char ch, color_t co)
{
	unsigned size = canvas->col * canvas->row;
	char tmp[37];
	charColorToESC(tmp,ch,co);
	for (int i=0; i < size ; i++)
	{
		memcpy(canvas->mem+(i*coloredChar_size), tmp, coloredChar_size);
	} 
}

/*
ornament something like this
o=-=-=-=##=-=-=-=o
"								 "
|                !
!                |
|								 !
!                |
o=-=-=-=##=-=-=-=o
"                "
!								 !
								 '
'                
								 '
` -  

it is 'responsive'
*/
void renderOrnament()
{
	size_t cC = coloredChar_size;
	char horizontal_bar[COL*cC];
	color_t col_o = COLOR(50,45,75,200,235,255);
	color_t col_b = COLOR(50,45,75,100,125,180);

	char tmp[37]; 
	for (int i=0; i<COL; i++)
	{
		if (i==0 || i==(COL-1))
		{	
			charColorToESC(tmp,'o',col_o);
			memcpy(horizontal_bar+(i*cC),tmp,cC);
		}
		else
		{
			charColorToESC(tmp,((i%2)==0) ? '-' : '=',rndColDev(col_b,5,5,5,5,5,25));
			memcpy(horizontal_bar+(i*cC),tmp,cC);
		}
	}
	memcpy(SCREEN_CANVAS.mem,horizontal_bar,COL*cC);
	memcpy(SCREEN_CANVAS.mem+(COL*(GAME_CANVAS.row+1)*cC),horizontal_bar,COL*cC);
	/*
	charColorToESC(tmp,'"',col_b);
	memcpy(SCREEN_CANVAS.mem+(cC*COL),tmp,cC);
	memcpy(SCREEN_CANVAS.mem+(cC*(2*COL-1)),tmp,cC);
	*/
	for(int i=0; i<GAME_CANVAS.row; i++)
	{
		charColorToESC(tmp,((i%2)==0) ? '!' : '|',rndColDev(col_b,5,5,5,5,5,25));
		memcpy(SCREEN_CANVAS.mem+((i+1)*cC*COL),tmp,cC);
		memcpy(SCREEN_CANVAS.mem+(((i+1)*COL + COL-1)*cC),tmp,cC);
	}
	
	for(int i=0; i<TEXT_CANVAS.row/2 ; i++)
	{
		charColorToESC(tmp,((i%2)==0) ? '!' : '|',rndColDev(col_b,5,5,5,5,5,25));
		memcpy(SCREEN_CANVAS.mem+((i+GAME_CANVAS.row+2)*COL*cC),tmp,cC);
		memcpy(SCREEN_CANVAS.mem+(((i+GAME_CANVAS.row+2)*COL + COL-1)*cC),tmp,cC);
	}
	
	int max = (TEXT_CANVAS.row/2) + (TEXT_CANVAS.row % 2);
	for(int i=0; i<max ; i++)
	{	
		for (int k=0; k < 3; col_b.bg[k++]-=(40/max)) // I am evil. 

		charColorToESC(tmp,(rand()%4)<3 ? '\'':' ',rndColDev(col_b,5,5,5,5,5,25));	
		memcpy(SCREEN_CANVAS.mem+(
					(i+GAME_CANVAS.row+2+(TEXT_CANVAS.row/2))*COL*cC)
				,tmp
				,cC);

		charColorToESC(tmp,(rand()%2)==0 ? '\'':' ',rndColDev(col_b,5,5,5,5,5,25));
		memcpy(SCREEN_CANVAS.mem+(
					((i+GAME_CANVAS.row+2+(TEXT_CANVAS.row/2))*COL + COL-1)*cC)
				,tmp
				,cC);
	}
	
	charColorToESC(tmp,'`',rndColDev(col_b,5,5,5,5,5,25));
	memcpy(SCREEN_CANVAS.mem+((ROW-1)*COL*cC),tmp,cC);
	charColorToESC(tmp,'-',rndColDev(col_b,5,5,5,5,5,25));
	memcpy(SCREEN_CANVAS.mem+((ROW-1)*COL*cC + cC),tmp,cC);
	for(int i=2; i<COL; i++)
	{
		charColorToESC(tmp,' ',rndColDev(col_b,5,5,5,5,5,25));
		memcpy(SCREEN_CANVAS.mem+(((ROW-1)*COL + i)*cC),tmp,cC);		
	} 	
}

/* TODO
void fadeToBlack(canvas_t* canvas)
{
	int size = canvas.row * canvas.col;
	int COND = 1;
	while (COND)
	{
		//todo
	} 
}*/
