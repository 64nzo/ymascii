/*Copyright 2023-2024 Anzo.
This file is part of the YMASCII PROJECT, and as such, is under the 
GPL-3.0-or-later licence.
Please refer to the LICENCE file for licence details.*/

#include <stdlib.h> 
#include <stdarg.h>

#include "messages.h"

#include "render.h"
#include "layers.h"
#include "globals.h"

static messages_queue_t MESSAGES_QUEUE;
unsigned MESSAGES_LENGTH;
unsigned TEXT_SCROLL = 0;//FIXME Shouldnt be able to scroll past last
												 //message too.

void initMessagesQueue()
{
	MESSAGES_LENGTH = TEXT_CANVAS.col; //counting trailing \n\0
	MESSAGES_QUEUE = NULL;
}

void debugVerifyIntergrityQueue()
{

	printf("\033[2J\033[0m\033[0H");
	int prev = -1;
	int i = 0;
	messages_queue_t mq = MESSAGES_QUEUE;
	while (mq != NULL)
	{
		printf("messages : %s rank : %i\n", mq->message, mq->rank);
		if (i > MAX_MESSAGES)
		{
			printf("Too much messages : %i\n !",i);
		} 
		if (prev == mq->rank)
		{
			printf(
	"ill-formated message queue : prev is %i and mq->rank is %i whilst it should be %i \n\n"
	,prev, mq->rank, i);
		}
		prev = mq->rank;
		mq = mq->next;
		i++;
	}
	//sleep(1);
} 

void updateMessageQueue()
{
	messages_queue_t mq = MESSAGES_QUEUE;
	if (mq == NULL){return;} //Nothing to do empty messages queue..
													
	while(mq != NULL)
	{
		(mq->rank) += 1;
		if (mq->next == NULL){return;}
		if ((mq->next)->rank+1 == MAX_MESSAGES)
		{
			free(mq->next->message);
			free(mq->next);
			mq->next = NULL;
			//debugVerifyIntergrityQueue();	
			return;
		}
		mq = mq->next;	
	}
	//debugVerifyIntergrityQueue();	
} 

void addMessagesToQueue(char* txt)
{
	messages_queue_t messageq = 
		(messages_queue_t) malloc(sizeof(struct messages_queue_t));
	
	size_t n = strlen(txt);
	n = (n > MESSAGES_LENGTH) ? MESSAGES_LENGTH : n; //n = max(MAX_MESSAGES,n);

	messageq->message = strndup(txt,n); //malloc underhood
	messageq->rank = 0;
	updateMessageQueue();
	messageq->next = MESSAGES_QUEUE;
	MESSAGES_QUEUE = messageq;

	if (n == MESSAGES_LENGTH)
	{
		addMessagesToQueue(txt+MESSAGES_LENGTH);
		n = MESSAGES_LENGTH;
	} 
	//debugVerifyIntergrityQueue();	
}

void sendMessage(char* mess)
{
	TEXT_SCROLL = 0;
	addMessagesToQueue(mess);
}

void sendMessageFmt(char *fmt, ...)
{
	char mess[255]; 	
	va_list args;
	va_start(args, fmt);
	vsnprintf(mess,255,fmt,args);
	va_end(args);
	sendMessage(mess);
} 

void blitMessages(unsigned scroll_pos)
{
	//get to wanted mq
	messages_queue_t mq = MESSAGES_QUEUE;
	if (mq == NULL){return;} //no messages.. 
	while ((mq->rank < scroll_pos) && (mq->next != NULL)){mq = mq->next;}
		
	size_t cC = coloredChar_size;
	size_t n; //length of mess
	char tmp[cC]; 
	int offcol = 0; //color offset
	
	int i;
	for (i=TEXT_CANVAS.row-1 ; i > -1 && mq != NULL ; i--)
	{
		n = strlen(mq->message);
		offcol = 200 - i*(200/(TEXT_CANVAS.row-1));
		color_t col = COLOR(0,0,0,200-offcol,255-offcol,255-offcol);
		for (int j=0; j<n; j++)
		{	
			charColorToESC(tmp, mq->message[j], col);
			memcpy(TEXT_CANVAS.mem+(i*TEXT_CANVAS.col*cC)+(j*cC), tmp, cC);
		}

		for (int j=n; j<TEXT_CANVAS.col; j++)//fill the rest with black char
		{
			charColorToESC(tmp, ' ', col);
			memcpy(TEXT_CANVAS.mem+(i*TEXT_CANVAS.col*cC)+(j*cC), tmp, cC);
		} 
		mq = mq->next;	
	}
	
	for (i=i; i > -1; i--) //fill the rest of line with nothing
	{
		for(int j=0; j < TEXT_CANVAS.col; j++)
		{
			charColorToESC(tmp, ' ', GRAY_ON_BLACK);
			memcpy(TEXT_CANVAS.mem+(i*TEXT_CANVAS.col*cC)+(j*cC), tmp, cC);
		} 		
	} 	
} 
