SRC=$(wildcard src/*.c)
HDR=$(wildcard src/*.h)
OBJ=$(subst obj/editor.o,,$(patsubst src/%,obj/%,$(SRC:%.c=%.o)))

DEP=$(OBJ:%.o=%.d) 
FLAGS=-g
NAME=out

# https://stackoverflow.com/questions/9598595/\
how-to-manage-c-header-file-dependencies
obj/%.o: src/%.c
	gcc -c -o $@ -MMD -MP -MF ${@:.o=.d} $< $(FLAGS)

all: mkdir $(OBJ)
	gcc $(OBJ) -o $(NAME) -g

run: all
	./out

clean:
	rm -rf obj/ edtobj/ $(NAME) $(EDITOR_NAME)

mkdir:
	mkdir -p obj/

-include $(DEP)

#EDITOR BUILD
EDITOR_NAME=editor
EDTOBJ=$(subst edtobj/main.o,,$(patsubst src/%,edtobj/%,$(SRC:%.c=%.o)))
EDTFLAGS=-g -DEDITOR

editor: mkdiredt $(EDTOBJ)
	gcc $(EDTOBJ) -o $(EDITOR_NAME) -g

edtobj/%.o: src/%.c
	gcc -c -o $@ -MMD -MP -MF ${@:.o=.d} $< $(EDTFLAGS)

mkdiredt:
	mkdir -p edtobj/

